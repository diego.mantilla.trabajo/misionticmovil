package com.example.xtremeapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.xtremeapp.daoimp.MarcadorDAO;
import com.example.xtremeapp.modelos.Marcador;
import com.example.xtremeapp.modelos.TipoLugar;
import com.google.android.gms.maps.model.LatLng;

import org.w3c.dom.Text;

public class frmMarcadorActivity extends AppCompatActivity implements LocationListener {
    int id = 0;
    Marcador mk = null;
    Location ubicacion = null;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Context contexto = this;

        setContentView(R.layout.activity_frm_marcador);

        //Solicita permisos para usar el gps
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if(!locationManager.isLocationEnabled())
            startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        //Solicita el seguimiento de la localizacion por GPS, INTERNET
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
        locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 0, 0, this);


        Spinner spTipo = (Spinner) findViewById(R.id.frmMarcador_spnTipo);
        spTipo.setAdapter(new ArrayAdapter<TipoLugar>(this, android.R.layout.simple_spinner_item, TipoLugar.values()));

        Button btnVolver = (Button) findViewById(R.id.frmMarcador_btnCancelar);
        Button btnGuardar = (Button) findViewById(R.id.frmMarcador_btnGuardar);

        TextView txtNombre = (TextView) findViewById(R.id.marcador_txtNombre);
        TextView txtDescripcion = (TextView) findViewById(R.id.frmMarcador_txtDescripcion);
        Spinner spnTipo = (Spinner) findViewById(R.id.frmMarcador_spnTipo);
        TextView txtLatitud = (TextView) findViewById(R.id.frmMarcador_spnLatitud);
        TextView txtLongitud = (TextView) findViewById(R.id.frmMarcador_txtLongitud);

        Bundle extras = getIntent().getExtras();
        try {
            if(extras!=null)
                id = extras.getInt("id");

        }
        catch (Exception ex)
        {}


        if(id>0) {
            MarcadorDAO mkado = new MarcadorDAO(this);
            mk = mkado.ver(id);
            txtNombre.setText(mk.getNombre());
            txtDescripcion.setText(mk.getDescripcion());
            spnTipo.setSelection(mk.getTipo().ordinal());
            txtLatitud.setText(String.valueOf(mk.getLatitud()));
            txtLongitud.setText(String.valueOf(mk.getLongitud()));
        }

        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MarcadorDAO mkado = new MarcadorDAO(contexto);
                boolean hecho = false;
                if(id>0)
                    hecho = mkado.editar(mk);
                else {
                    mk = new Marcador(0,txtNombre.getText().toString(),txtDescripcion.getText().toString(), Double.parseDouble(txtLatitud.getText().toString()), Double.parseDouble(txtLongitud.getText().toString()), (TipoLugar) spnTipo.getSelectedItem());
                    hecho = mkado.insertar(mk) > 0;
                }
                if(hecho)
                {
                    Toast.makeText(contexto, "Se ha guardado correctamente el registro!", Toast.LENGTH_LONG).show();
                    onBackPressed();
                }
                else
                    Toast.makeText(contexto, "Ocurrio un error al intentar guardar el registro.", Toast.LENGTH_LONG).show();
            }
        });

        ImageButton btnObtenerUbicacion = (ImageButton) findViewById(R.id.marcador_btnObtenerUbicacion);
        btnObtenerUbicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    txtLatitud.setText(String.valueOf(ubicacion.getLatitude()));
                    txtLongitud.setText(String.valueOf(ubicacion.getLongitude()));
                }
                catch (Exception ex)
                {

                }
            }
        });

    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        this.ubicacion = location;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(@NonNull String provider) {

    }

    @Override
    public void onProviderDisabled(@NonNull String provider) {

    }
}