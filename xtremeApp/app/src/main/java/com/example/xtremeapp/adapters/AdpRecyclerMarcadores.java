package com.example.xtremeapp.adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.xtremeapp.HomeActivity;
import com.example.xtremeapp.R;
import com.example.xtremeapp.daoimp.MarcadorDAO;
import com.example.xtremeapp.frmMarcadorActivity;
import com.example.xtremeapp.marcadoresActivity;
import com.example.xtremeapp.modelos.Marcador;
import java.util.ArrayList;
import androidx.annotation.NonNull;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

public class AdpRecyclerMarcadores extends RecyclerView.Adapter<AdpRecyclerMarcadores.ViewHolderRegistro> {

    ArrayList<Marcador> registros;

    public AdpRecyclerMarcadores(ArrayList<Marcador> registros) {
        this.registros = registros;
    }

    @NonNull
    @Override
    public ViewHolderRegistro onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.reciclerviewitem_marcadores,null,false);
        return new ViewHolderRegistro(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderRegistro holder, int position) {
        holder.asignarRegistro(registros.get(position));
    }

    @Override
    public int getItemCount() {
        return this.registros.size();
    }

    public class ViewHolderRegistro extends RecyclerView.ViewHolder {

        int id;
        TextView nombre;
        TextView descripcion;
        TextView tipo;

        public ViewHolderRegistro(@NonNull View itemView) {
            super(itemView);
            nombre = (TextView) itemView.findViewById(R.id.itm_nombre);
            descripcion = (TextView) itemView.findViewById(R.id.itm_descripcion);
            tipo = (TextView) itemView.findViewById(R.id.itm_tipo);

            ImageButton btnEditar = (ImageButton) itemView.findViewById(R.id.itm_btnEditar);
            ImageButton btnEliminar = (ImageButton) itemView.findViewById(R.id.itm_btnEliminar);

            btnEditar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent activity = new Intent(view.getContext(), frmMarcadorActivity.class);
                    activity.putExtra("id", id);
                    view.getContext().startActivity(activity);
                }
            });

            btnEliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MarcadorDAO articuloado = new MarcadorDAO(itemView.getContext());
                    if(articuloado.eliminar(id)) {
                        Toast.makeText(view.getContext(), "Se ha eliminado el registro.", Toast.LENGTH_LONG).show();
                        view.getContext().startActivity(new Intent(view.getContext(), marcadoresActivity.class));
                    }
                    else {
                        Toast.makeText(view.getContext(), "Se ha producido un error al intentar eliminar el registro.", Toast.LENGTH_LONG).show();
                    }
                }
            });

        }

        public void asignarRegistro(Marcador registro) {
            nombre.setText(registro.getNombre());
            descripcion.setText(registro.getDescripcion());
            tipo.setBackgroundResource(registro.getTipo().getImagen());
            id = registro.getId();
        }
    }
}
