package com.example.xtremeapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Context contexto = this;

        try {
            Bundle variables = getIntent().getExtras();
            String email = variables.getString("email");

        }
        catch (Exception ex)
        {

        }
        //Recupera las variables enviadas desde el login

        CardView btnMarcadores = (CardView) findViewById(R.id.btnMarcador);
        CardView btnVerMapa = (CardView) findViewById(R.id.btnMapa);
        CardView btnAcercade = (CardView) findViewById(R.id.btnAcercade);
        CardView btnSalir = (CardView) findViewById(R.id.btnVolver);

        btnMarcadores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent activity = new Intent(contexto, marcadoresActivity.class);
                startActivity(activity);
            }
        });

        btnVerMapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent activity = new Intent(contexto, MapaActivity.class);
                startActivity(activity);
            }
        });

        btnAcercade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent activity = new Intent(contexto, AcercaDeActivity.class);
                startActivity(activity);
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                onBackPressed();
            }
        });
    }
}