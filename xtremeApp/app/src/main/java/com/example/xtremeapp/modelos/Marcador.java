package com.example.xtremeapp.modelos;

import android.content.Context;

import com.example.xtremeapp.R;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class Marcador {
    private int id;
    private String nombre;
    private String descripcion;
    private double latitud;
    private double longitud;
    private TipoLugar tipo;

    public Marcador(int id, String nombre, String descripcion, double latitud, double longitud, TipoLugar tipo) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.latitud = latitud;
        this.longitud = longitud;
        this.tipo = tipo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public TipoLugar getTipo() {
        return tipo;
    }

    public void setTipo(TipoLugar tipo) {
        this.tipo = tipo;
    }

    public MarkerOptions getMarcador(Context contexto)
    {
        MarkerOptions mk = new MarkerOptions();

        LatLng posicion = new LatLng(this.latitud, this.longitud);
        mk.icon(this.tipo.getImagen(contexto));
        mk.position(posicion);
        mk.title(this.nombre);

        return mk;
    }

    public LatLng getPosicion()
    {
        return new LatLng(this.latitud, this.longitud);
    }
}
