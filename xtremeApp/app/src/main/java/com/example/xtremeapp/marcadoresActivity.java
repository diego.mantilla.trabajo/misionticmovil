package com.example.xtremeapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.xtremeapp.adapters.AdpRecyclerMarcadores;
import com.example.xtremeapp.daoimp.MarcadorDAO;
import com.example.xtremeapp.modelos.Marcador;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

public class marcadoresActivity extends AppCompatActivity {

    private Context contexto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        contexto = this;

        setContentView(R.layout.activity_marcadores);
        actualizarRecycler();

        ImageButton btnBuscar = (ImageButton) findViewById(R.id.btnBuscar);
        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actualizarRecycler();
            }
        });

        FloatingActionButton btnNuevo = (FloatingActionButton) findViewById(R.id.recyclerview_btnNuevo);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent nuevoMarcador = new Intent(contexto, frmMarcadorActivity.class);
                startActivity(nuevoMarcador);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        actualizarRecycler();
    }

    private void actualizarRecycler()
    {
        TextView txtBuscar = (TextView) findViewById(R.id.txt_criterio);
        RecyclerView  recyclerMarcadores = (RecyclerView) findViewById(R.id.rcwMarcadores);
        recyclerMarcadores.setLayoutManager(new LinearLayoutManager(this));
        MarcadorDAO registros = new MarcadorDAO(this);
        AdpRecyclerMarcadores adapterArticulos = new AdpRecyclerMarcadores(registros.mostrar(txtBuscar.getText().toString()));
        recyclerMarcadores.setAdapter(adapterArticulos);
    }


}