package com.example.xtremeapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            Thread.sleep(2000);
            setTheme(R.style.Theme_Xtremeapp);
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Asigna los componentes a su respectiva variable
        TextView txtEmail = (TextView) findViewById(R.id.txtEmail);
        TextView txtClave = (TextView) findViewById(R.id.txtClave);


        //Crea los eventos de acceso y registro
        Button btnRegistro = (Button) findViewById(R.id.btnRegistro);
        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registrar(txtEmail.getText().toString(), txtClave.getText().toString());
            }
        });
        Button btnAcceder = (Button) findViewById(R.id.btnAcceder);
        btnAcceder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                acceder(txtEmail.getText().toString(), txtClave.getText().toString());
            }
        });
    }

    private void registrar(String email, String clave)
    {
        if(!email.isEmpty() && !clave.isEmpty())
        {
            FirebaseAuth.getInstance().
                    createUserWithEmailAndPassword(email, clave).addOnCompleteListener(r->{
                        if(r.isSuccessful())
                            this.msgExitoso(email);
                        else
                            this.msgError();
                    });
        }
    }

    private void acceder(String email, String clave)
    {
        if(!email.isEmpty() && !clave.isEmpty())
        {
            FirebaseAuth.getInstance().
                    signInWithEmailAndPassword(email, clave).addOnCompleteListener(r->{
                if(r.isSuccessful())
                    this.msgExitoso(email);
                else
                    this.msgError();
            });
        }
    }

    private void msgError()
    {
        AlertDialog.Builder mensaje = new AlertDialog.Builder(this);
        mensaje.setTitle("Error");
        mensaje.setMessage("Se ha producido un error autenticando al usuario");
        mensaje.setPositiveButton("Aceptar", null);
        mensaje.create();
        mensaje.show();
    }

    private void msgExitoso(String email)
    {
        Intent home = new Intent(this, HomeActivity.class);
//          Intent home = new Intent(this, MapaActivity.class);
        home.putExtra("email", email);
        startActivity(home);
    }
}