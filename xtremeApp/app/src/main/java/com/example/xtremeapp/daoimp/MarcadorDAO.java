package com.example.xtremeapp.daoimp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.xtremeapp.modelos.Marcador;
import com.example.xtremeapp.modelos.TipoLugar;

import java.util.ArrayList;
import java.util.Arrays;

import androidx.annotation.Nullable;

public class MarcadorDAO extends SqliteBDConex {

    Context context;

    public MarcadorDAO(@Nullable Context context) {
        super(context);
        this.context = context;
    }

    public long insertar(Marcador marcadorDAO) {

        long id = 0;

        try {
            SqliteBDConex dbHelper = new SqliteBDConex(context);
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put("nombre", marcadorDAO.getNombre());
            values.put("descripcion", marcadorDAO.getDescripcion());
            values.put("latitud", marcadorDAO.getLatitud());
            values.put("longitud", marcadorDAO.getLongitud());
            values.put("tipo", marcadorDAO.getTipo().getValor());
            id = db.insert(TABLE, null, values);
        } catch (Exception ex) {
            ex.toString();
        }

        return id;
    }

    public ArrayList<Marcador> mostrar(@Nullable String criterio) {

        SqliteBDConex dbHelper = new SqliteBDConex(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ArrayList<Marcador> listaRegistros = new ArrayList<>();
        Marcador registro;
        Cursor cursorRegistros;
        String query = "SELECT * FROM " + TABLE;

        if(criterio!=null) {
            try {
                criterio = String.valueOf(Integer.parseInt(criterio));
                query += " WHERE id = " + criterio;
            } catch (Exception ex) {
                query += " WHERE nombre like '%" + criterio + "%'";
            }
        }
        cursorRegistros = db.rawQuery(query, null);

        if (cursorRegistros.moveToFirst()) {
            do {
                TipoLugar tlugar = null;
                String lugar = cursorRegistros.getString(3);
                for (TipoLugar value : TipoLugar.values()) {
                    if(value.getValor().equals(cursorRegistros.getString(3)))
                        tlugar = value;
                }

                registro = new Marcador(
                        cursorRegistros.getInt(0),
                        cursorRegistros.getString(1),
                        cursorRegistros.getString(2),
                        cursorRegistros.getDouble(4),
                        cursorRegistros.getDouble(5),
                        tlugar
                );
                listaRegistros.add(registro);
            } while (cursorRegistros.moveToNext());
        }

        cursorRegistros.close();

        return listaRegistros;
    }

    public Marcador ver(int id) {

        SqliteBDConex dbHelper = new SqliteBDConex(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        Marcador registro = null;
        Cursor cursorRegistros;

        cursorRegistros = db.rawQuery("SELECT * FROM " + TABLE + " WHERE id = " + id + " LIMIT 1", null);

        if (cursorRegistros.moveToFirst()) {
            TipoLugar tlugar = null;
            for (TipoLugar value : TipoLugar.values()) {
                if(value.getValor().equals(cursorRegistros.getString(3)))
                    tlugar = value;
            }
            registro = new Marcador(
                    cursorRegistros.getInt(0),
                    cursorRegistros.getString(1),
                    cursorRegistros.getString(2),
                    cursorRegistros.getDouble(4),
                    cursorRegistros.getDouble(5),
                    tlugar
            );
        }

        cursorRegistros.close();

        return registro;
    }

    public boolean editar(Marcador m) {

        boolean correcto = false;

        SqliteBDConex dbHelper = new SqliteBDConex(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        try {
            db.execSQL("UPDATE " + TABLE + " SET nombre = '" + m.getNombre() + "', descripcion = '" + m.getDescripcion() + "', tipo = '" + m.getTipo().getValor() + "', latitud = " + m.getLatitud() + ", longitud = " + m.getLongitud() + " WHERE id=" + m.getId());
            correcto = true;
        } catch (Exception ex) {
            ex.toString();
            correcto = false;
        } finally {
            db.close();
        }

        return correcto;
    }

    public boolean eliminar(int id) {

        boolean correcto = false;

        SqliteBDConex dbHelper = new SqliteBDConex(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        try {
            db.execSQL("DELETE FROM " + TABLE + " WHERE id = '" + id + "'");
            correcto = true;
        } catch (Exception ex) {
            ex.toString();
            correcto = false;
        } finally {
            db.close();
        }

        return correcto;
    }
}

