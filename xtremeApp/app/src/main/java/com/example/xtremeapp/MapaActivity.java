package com.example.xtremeapp;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;

import com.example.xtremeapp.adapters.AdpRecyclerMarcadores;
import com.example.xtremeapp.daoimp.MarcadorDAO;
import com.example.xtremeapp.modelos.Marcador;
import com.example.xtremeapp.modelos.TipoLugar;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.MarkerOptions;
import com.example.xtremeapp.databinding.ActivityMapaBinding;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MapaActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener {

    private GoogleMap mMap;
    private ActivityMapaBinding binding;
    protected MarkerOptions marcadorActual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Context contexto = this;

        binding = ActivityMapaBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        FloatingActionButton btnVolver = (FloatingActionButton) findViewById(R.id.btnVolver);
        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        FloatingActionButton btnNuevo = (FloatingActionButton) findViewById(R.id.btnNuevaLocalizacion);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent nuevoMarcador = new Intent(contexto, frmMarcadorActivity.class);
                startActivity(nuevoMarcador);
            }
        });



    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @SuppressLint("NewApi")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        MarcadorDAO registros = new MarcadorDAO(this);
        registros.mostrar(null).forEach(r->{
            mMap.addMarker(r.getMarcador(this));
        });
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0, this);
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {

        Marcador posActual = new Marcador(0, "Posición Actual", "Posición Actual", location.getLatitude(), location.getLongitude(), TipoLugar.posactual);

        if(marcadorActual==null) {
            marcadorActual = new MarkerOptions();
            marcadorActual.position(posActual.getPosicion());
            marcadorActual.title(posActual.getNombre());
            marcadorActual.icon(posActual.getTipo().getImagen(this));
            mMap.setMinZoomPreference(15);
            mMap.setMaxZoomPreference(20);
            mMap.addMarker(marcadorActual);
        }
        else
            marcadorActual.position(posActual.getPosicion());

        mMap.moveCamera(CameraUpdateFactory.newLatLng(marcadorActual.getPosition()));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(@NonNull String provider) {

    }

    @Override
    public void onProviderDisabled(@NonNull String provider) {

    }

}