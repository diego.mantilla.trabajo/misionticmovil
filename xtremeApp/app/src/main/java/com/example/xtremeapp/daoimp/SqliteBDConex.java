package com.example.xtremeapp.daoimp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class SqliteBDConex extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NOMBRE = "xtremeapp.db";
    public static final String TABLE = "marcadores";

    public SqliteBDConex(@Nullable Context context) {
        super(context, DATABASE_NOMBRE, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL("CREATE TABLE marcadores (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nombre VARCHAR (100) NOT NULL, descripcion TEXT, tipo VARCHAR (45) NOT NULL, latitud DECIMAL (4, 8) NOT NULL, longitud DECIMAL (4, 8) NOT NULL)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        sqLiteDatabase.execSQL("DROP TABLE " + TABLE);
        onCreate(sqLiteDatabase);

    }

}
